const db = require("../models");
const Test = db.test;
const Op = db.Sequelize.Op;
const testService = require("../services/testService");

exports.get = function (req, res) {
  const respo = testService.getRows();
  respo
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the test.",
      });
    });
};

exports.create = function (req, res) {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a test object
  const test = {
    title: req.body.title,
    description: req.body.description,
    published: req.body.published ? req.body.published : false,
  };

  // Save the test object in the database
  Test.create(test)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the test.",
      });
    });
};
