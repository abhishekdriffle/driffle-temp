const express = require("express");
const router = express.Router();

var testConrtroller = require("../../controllers/testController");

router.post("/", testConrtroller.create);
router.get("/", testConrtroller.get);

module.exports = router;
