const { Pool } = require("pg");
const config = require("../config/pg.config");
const pool = new Pool(config.db);

async function query(query, params) {
  const { rows, fields } = await pool.query(query);
  return rows;
}

module.exports = {
  query,
};
