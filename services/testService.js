const db = require("./db");
const helper = require("../helper/helper");

async function getRows() {
  const rows = await db.query("SELECT id, title, description FROM tests");
  return rows;
}

module.exports = {
  getRows,
};
